# Good Morning application 


All the api function is in HttpHelper.java

The base url for api is located at string.xml under the name base_url

All database insertion, update delete and selection is located at CRUDHelper.java

All create table sql is located at DBHelper.java



Build apk
----------------
The key store path must pin point to goodmorning.jks in goodmorning folder.

The key store password is abc123, the key alias is abc123, the key password is abc123..

The version name, version code is programmed in build.gradle (Module:app).



Api Reference
----------------
http://203.223.143.30:8070/gdmorningapi/help/



Payment Link Web
-----------------
http://uatserver.com.my/goodmorning/ShoppingCart/PostToPayPal?orderId={orderId}&token={url encoded token}


Library Reference
------------------
1. https://github.com/flavienlaurent/datetimepicker
2. https://github.com/bumptech/glide
3. https://github.com/umano/AndroidSlidingUpPanel
4. https://github.com/JodaOrg/joda-time


Design Flow:
-----------------
![](http://35.197.160.118/wp-content/uploads/2018/08/GM-Mobile-App_Updated_by_070416.jpg)