package com.applab.goodmorning.Event.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.goodmorning.Event.model.Schedule;
import com.applab.goodmorning.Event.model.ScheduleItems;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.AppController;
import com.applab.goodmorning.Utilities.GsonRequest;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Utilities.dialog.GeneralDialogFragment;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by user on 1/4/2016.
 */
public class HttpHelper {
    public static void getEventList(Context context, String TAG, String startDate, String endDate) {
        Utilities.sendNormalLock(true, context, TAG);
        GsonRequest<ScheduleItems> mGsonRequest = new GsonRequest<ScheduleItems>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Event/List/Schedule?countryId=" + Utilities.getCountryID(context) + "&fromDate="
                        + startDate + "&toDate=" + endDate + "",
                ScheduleItems.class,
                null,
                responseEventListListener(context, TAG),
                errorEventListListener(context, TAG)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ScheduleItems> responseEventListListener(final Context context, final String TAG) {
        return new Response.Listener<ScheduleItems>() {
            @Override
            public void onResponse(ScheduleItems response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("Schedules", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }

            }
        };
    }

    public static Response.ErrorListener errorEventListListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void getEvent(Context context, String TAG, Schedule schedule) {
        Utilities.sendNormalLock(true, context, TAG);
        GsonRequest<ScheduleItems> mGsonRequest = new GsonRequest<ScheduleItems>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Event/Single?eventId=" + schedule.getEventId().toString(),
                ScheduleItems.class,
                null,
                responseEventListener(context, TAG),
                errorEventListener(context, TAG)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ScheduleItems> responseEventListener(final Context context, final String TAG) {
        return new Response.Listener<ScheduleItems>() {
            @Override
            public void onResponse(ScheduleItems response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("Schedule", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorEventListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void getMonth(Context context, String TAG, String startDate, String endDates) {
        Utilities.sendNormalLock(true, context, TAG);
        GsonRequest<ScheduleItems> mGsonRequest = new GsonRequest<ScheduleItems>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Event/List/Month?countryId=" + Utilities.getCountryID(context) + "&fromDate=" + startDate + "&toDate=" + endDates,
                ScheduleItems.class,
                null,
                responseMonthListener(context, TAG),
                errorMonthListener(context, TAG)) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ScheduleItems> responseMonthListener(final Context context, final String TAG) {
        return new Response.Listener<ScheduleItems>() {
            @Override
            public void onResponse(ScheduleItems response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("Schedules", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorMonthListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
