package com.applab.goodmorning.Gallery.model;

/**
 * Created by user on 1/4/2016.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Album implements Parcelable {

    @SerializedName("AlbumId")
    @Expose
    private Integer AlbumId;
    @SerializedName("CoverPhoto")
    @Expose
    private String CoverPhoto;
    @SerializedName("AlbumTitle")
    @Expose
    private String AlbumTitle;
    @SerializedName("PhotoCount")
    @Expose
    private Integer PhotoCount;
    @SerializedName("Date")
    @Expose
    private String Date;
    @SerializedName("AlbumDescription")
    @Expose
    private String AlbumDescription;
    @SerializedName("Photo")
    @Expose
    private ArrayList<Photo> Photo = new ArrayList<Photo>();

    /**
     * @return The AlbumDescription
     */
    public String getAlbumDescription() {
        return AlbumDescription;
    }

    /**
     * @param AlbumDescription The AlbumDescription
     */
    public void setAlbumDescription(String AlbumDescription) {
        this.AlbumDescription = AlbumDescription;
    }

    /**
     * @return The Photo
     */
    public ArrayList<Photo> getPhoto() {
        return Photo;
    }

    /**
     * @param Photo The Photo
     */
    public void setPhoto(ArrayList<Photo> Photo) {
        this.Photo = Photo;
    }


    /**
     * @return The Date
     */
    public String getDate() {
        return Date;
    }

    /**
     * @param Date The Date
     */
    public void setDate(String Date) {
        this.Date = Date;
    }


    /**
     * @return The AlbumId
     */
    public Integer getAlbumId() {
        return AlbumId;
    }

    /**
     * @param AlbumId The AlbumId
     */
    public void setAlbumId(Integer AlbumId) {
        this.AlbumId = AlbumId;
    }

    /**
     * @return The CoverPhoto
     */
    public String getCoverPhoto() {
        return CoverPhoto;
    }

    /**
     * @param CoverPhoto The CoverPhoto
     */
    public void setCoverPhoto(String CoverPhoto) {
        this.CoverPhoto = CoverPhoto;
    }

    /**
     * @return The AlbumTitle
     */
    public String getAlbumTitle() {
        return AlbumTitle;
    }

    /**
     * @param AlbumTitle The AlbumTitle
     */
    public void setAlbumTitle(String AlbumTitle) {
        this.AlbumTitle = AlbumTitle;
    }

    /**
     * @return The PhotoCount
     */
    public Integer getPhotoCount() {
        return PhotoCount;
    }

    /**
     * @param PhotoCount The PhotoCount
     */
    public void setPhotoCount(Integer PhotoCount) {
        this.PhotoCount = PhotoCount;
    }

    public void readFromParcel(Parcel in) {
        this.AlbumId = in.readInt();
        this.CoverPhoto = in.readString();
        this.AlbumTitle = in.readString();
        this.PhotoCount = in.readInt();
    }

    public Album() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.AlbumId);
        dest.writeString(this.CoverPhoto);
        dest.writeString(this.AlbumTitle);
        dest.writeValue(this.PhotoCount);
        dest.writeString(this.Date);
        dest.writeString(this.AlbumDescription);
        dest.writeTypedList(Photo);
    }

    protected Album(Parcel in) {
        this.AlbumId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.CoverPhoto = in.readString();
        this.AlbumTitle = in.readString();
        this.PhotoCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Date = in.readString();
        this.AlbumDescription = in.readString();
        this.Photo = in.createTypedArrayList(com.applab.goodmorning.Gallery.model.Photo.CREATOR);
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel source) {
            return new Album(source);
        }

        @Override
        public Album[] newArray(int size) {
            return new Album[size];
        }
    };
}