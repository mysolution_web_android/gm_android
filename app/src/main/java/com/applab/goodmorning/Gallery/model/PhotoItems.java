package com.applab.goodmorning.Gallery.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 1/4/2016.
 */
public class PhotoItems implements Parcelable {

    @SerializedName("SystemCode")
    @Expose
    private Integer SystemCode;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("SystemMessage")
    @Expose
    private String SystemMessage;
    @SerializedName("SystemDebugMessage")
    @Expose
    private String SystemDebugMessage;
    @SerializedName("items")
    @Expose
    private List<Album> items = new ArrayList<Album>();

    /**
     * @return The SystemCode
     */
    public Integer getSystemCode() {
        return SystemCode;
    }

    /**
     * @param SystemCode The SystemCode
     */
    public void setSystemCode(Integer SystemCode) {
        this.SystemCode = SystemCode;
    }

    /**
     * @return The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     * @param PageNo The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     * @return The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     * @param NoPerPage The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     * @return The SystemMessage
     */
    public String getSystemMessage() {
        return SystemMessage;
    }

    /**
     * @param SystemMessage The SystemMessage
     */
    public void setSystemMessage(String SystemMessage) {
        this.SystemMessage = SystemMessage;
    }

    /**
     * @return The SystemDebugMessage
     */
    public String getSystemDebugMessage() {
        return SystemDebugMessage;
    }

    /**
     * @param SystemDebugMessage The SystemDebugMessage
     */
    public void setSystemDebugMessage(String SystemDebugMessage) {
        this.SystemDebugMessage = SystemDebugMessage;
    }

    /**
     * @return The items
     */
    public List<Album> getAlbums() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setAlbums(List<Album> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.SystemCode);
        dest.writeValue(this.PageNo);
        dest.writeValue(this.NoPerPage);
        dest.writeString(this.SystemMessage);
        dest.writeString(this.SystemDebugMessage);
        dest.writeTypedList(items);
    }

    public PhotoItems() {
    }

    protected PhotoItems(Parcel in) {
        this.SystemCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PageNo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.NoPerPage = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SystemMessage = in.readString();
        this.SystemDebugMessage = in.readString();
        this.items = in.createTypedArrayList(Album.CREATOR);
    }

    public static final Parcelable.Creator<PhotoItems> CREATOR = new Parcelable.Creator<PhotoItems>() {
        @Override
        public PhotoItems createFromParcel(Parcel source) {
            return new PhotoItems(source);
        }

        @Override
        public PhotoItems[] newArray(int size) {
            return new PhotoItems[size];
        }
    };
}

