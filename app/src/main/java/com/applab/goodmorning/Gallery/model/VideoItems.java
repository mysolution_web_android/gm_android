package com.applab.goodmorning.Gallery.model;

/**
 * Created by user on 30/3/2016.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoItems implements Parcelable {
    @SerializedName("SystemCode")
    @Expose
    private Integer SystemCode;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("SystemMessage")
    @Expose
    private String SystemMessage;
    @SerializedName("SystemDebugMessage")
    @Expose
    private String SystemDebugMessage;
    @SerializedName("items")
    @Expose
    private ArrayList<Video> items = new ArrayList<Video>();

    public Integer getSystemCode() {
        return SystemCode;
    }

    public void setSystemCode(Integer systemCode) {
        SystemCode = systemCode;
    }

    public Integer getPageNo() {
        return PageNo;
    }

    public void setPageNo(Integer pageNo) {
        PageNo = pageNo;
    }

    public Integer getNoPerPage() {
        return NoPerPage;
    }

    public void setNoPerPage(Integer noPerPage) {
        NoPerPage = noPerPage;
    }

    public String getSystemMessage() {
        return SystemMessage;
    }

    public void setSystemMessage(String systemMessage) {
        SystemMessage = systemMessage;
    }

    public String getSystemDebugMessage() {
        return SystemDebugMessage;
    }

    public void setSystemDebugMessage(String systemDebugMessage) {
        SystemDebugMessage = systemDebugMessage;
    }

    /**
     * @return The items
     */
    public ArrayList<Video> getVideos() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setVideos(ArrayList<Video> items) {
        this.items = items;
    }

    public void readFromParcel(Parcel in) {
        this.items = in.readArrayList(Video.class.getClassLoader());
    }

    public VideoItems() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.SystemCode);
        dest.writeValue(this.PageNo);
        dest.writeValue(this.NoPerPage);
        dest.writeString(this.SystemMessage);
        dest.writeString(this.SystemDebugMessage);
        dest.writeTypedList(items);
    }

    protected VideoItems(Parcel in) {
        this.SystemCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PageNo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.NoPerPage = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SystemMessage = in.readString();
        this.SystemDebugMessage = in.readString();
        this.items = in.createTypedArrayList(Video.CREATOR);
    }

    public static final Creator<VideoItems> CREATOR = new Creator<VideoItems>() {
        @Override
        public VideoItems createFromParcel(Parcel source) {
            return new VideoItems(source);
        }

        @Override
        public VideoItems[] newArray(int size) {
            return new VideoItems[size];
        }
    };
}