package com.applab.goodmorning.Gallery.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/4/2016.
 */
public class Photo implements Parcelable {

    @SerializedName("AlbumId")
    @Expose
    private Integer AlbumId;
    @SerializedName("Photo")
    @Expose
    private String Photo;

    /**
     *
     * @return
     * The AlbumId
     */
    public Integer getAlbumId() {
        return AlbumId;
    }

    /**
     *
     * @param AlbumId
     * The AlbumId
     */
    public void setAlbumId(Integer AlbumId) {
        this.AlbumId = AlbumId;
    }

    /**
     *
     * @return
     * The Photo
     */
    public String getPhoto() {
        return Photo;
    }

    /**
     *
     * @param Photo
     * The Photo
     */
    public void setPhoto(String Photo) {
        this.Photo = Photo;
    }

    public void readFromParcel(Parcel in) {
        this.AlbumId = in.readInt();
        this.Photo = in.readString();
    }

    public Photo() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.AlbumId);
        dest.writeString(this.Photo);
    }

    protected Photo(Parcel in) {
        this.AlbumId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Photo = in.readString();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };
}