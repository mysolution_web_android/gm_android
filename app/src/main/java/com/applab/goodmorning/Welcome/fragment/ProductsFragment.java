package com.applab.goodmorning.Welcome.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.applab.goodmorning.Order.model.OrderItem;
import com.applab.goodmorning.Products.model.Product;
import com.applab.goodmorning.Products.model.ProductItems;
import com.applab.goodmorning.Products.webapi.HttpHelper;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Register.model.Country;
import com.applab.goodmorning.Register.provider.CountryProvider;
import com.applab.goodmorning.Utilities.DBHelper;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Welcome.activity.WelcomeActivity;
import com.applab.goodmorning.Welcome.adapter.ProductsAdapter;
import com.applab.goodmorning.Welcome.model.Products;

import java.util.ArrayList;

/**
 * Created by user on 04-Mar-16.
 */
public class ProductsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private ProductsAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerview;
    ArrayList<Product> mProducts = new ArrayList<>();
    private static final String TAG = ProductsFragment.class.getSimpleName();
    private View mView;
    private int mLoaderCountryId = 569;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_products, container, false);
        mRecyclerview = (RecyclerView) mView.findViewById(R.id.myRecyclerView);
        mRecyclerview.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAdapter = new ProductsAdapter(getActivity(), mProducts);
        mRecyclerview.setAdapter(mAdapter);
        mRecyclerview.setLayoutManager(mLayoutManager);
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setDelay();
        com.applab.goodmorning.Register.webapi.HttpHelper.getCountry(getActivity(), TAG);
        getActivity().getSupportLoaderManager().initLoader(mLoaderCountryId, null, ProductsFragment.this);
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getActivity().getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    public void setDelay() {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                HttpHelper.getProductListing(getActivity(), TAG);
            }
        };
        handler.postDelayed(r, 200);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity().getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getParcelableExtra("Products") != null) {
                    ProductItems product = intent.getParcelableExtra("Products");
                    if (product != null) {
                        if (product.getItems() != null) {
                            mProducts = new ArrayList<>(product.getItems());
                            mAdapter.swapProducts(mProducts);
                        }
                    }
                } else if (intent.getIntExtra("isNormalLock", 2) == 1) {
                    Utilities.setNormalLock(true, getActivity(), mView.findViewById(R.id.fadeProgress));
                } else if (intent.getIntExtra("isNormalLock", 2) == 0) {
                    Utilities.setNormalLock(false, getActivity(), mView.findViewById(R.id.fadeProgress));
                }
            }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderCountryId == id) {
            return new CursorLoader(getActivity(), CountryProvider.CONTENT_URI, null, DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(Utilities.getCountryID(getActivity()))}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == mLoaderCountryId) {
            if (data != null) {
                if (data.getCount() > 0) {
                    Country country = Country.getCountry(data, 0);
                    mAdapter.swapCountry(country);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
