package com.applab.goodmorning.ContactUs.model;

/**
 * Created by user on 8/4/2016.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUsItem implements Parcelable {
    @SerializedName("SystemCode")
    @Expose
    private Integer SystemCode;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("SystemMessage")
    @Expose
    private String SystemMessage;
    @SerializedName("SystemDebugMessage")
    @Expose
    private String SystemDebugMessage;
    @SerializedName("items")
    @Expose
    private List<ContactUs> items = new ArrayList<ContactUs>();

    /**
     *
     * @return
     * The SystemCode
     */
    public Integer getSystemCode() {
        return SystemCode;
    }

    /**
     *
     * @param SystemCode
     * The SystemCode
     */
    public void setSystemCode(Integer SystemCode) {
        this.SystemCode = SystemCode;
    }

    /**
     *
     * @return
     * The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     *
     * @param PageNo
     * The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     *
     * @return
     * The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     *
     * @param NoPerPage
     * The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     *
     * @return
     * The SystemMessage
     */
    public String getSystemMessage() {
        return SystemMessage;
    }

    /**
     *
     * @param SystemMessage
     * The SystemMessage
     */
    public void setSystemMessage(String SystemMessage) {
        this.SystemMessage = SystemMessage;
    }

    /**
     *
     * @return
     * The SystemDebugMessage
     */
    public String getSystemDebugMessage() {
        return SystemDebugMessage;
    }

    /**
     *
     * @param SystemDebugMessage
     * The SystemDebugMessage
     */
    public void setSystemDebugMessage(String SystemDebugMessage) {
        this.SystemDebugMessage = SystemDebugMessage;
    }

    /**
     * @return The items
     */
    public List<ContactUs> getContactUss() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setContactUss(List<ContactUs> items) {
        this.items = items;
    }

    public void readFromParcel(Parcel in) {
        this.items = in.readArrayList(ContactUs.class.getClassLoader());
    }

    public ContactUsItem() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.SystemCode);
        dest.writeValue(this.PageNo);
        dest.writeValue(this.NoPerPage);
        dest.writeString(this.SystemMessage);
        dest.writeString(this.SystemDebugMessage);
        dest.writeTypedList(items);
    }

    protected ContactUsItem(Parcel in) {
        this.SystemCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PageNo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.NoPerPage = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SystemMessage = in.readString();
        this.SystemDebugMessage = in.readString();
        this.items = in.createTypedArrayList(ContactUs.CREATOR);
    }

    public static final Creator<ContactUsItem> CREATOR = new Creator<ContactUsItem>() {
        @Override
        public ContactUsItem createFromParcel(Parcel source) {
            return new ContactUsItem(source);
        }

        @Override
        public ContactUsItem[] newArray(int size) {
            return new ContactUsItem[size];
        }
    };
}