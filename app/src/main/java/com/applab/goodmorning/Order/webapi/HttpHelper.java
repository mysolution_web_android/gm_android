package com.applab.goodmorning.Order.webapi;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.goodmorning.Login.database.CRUDHelper;
import com.applab.goodmorning.Login.model.Token;
import com.applab.goodmorning.Order.model.OrderItem;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.AppController;
import com.applab.goodmorning.Utilities.GsonRequest;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Utilities.dialog.GeneralDialogFragment;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 8/4/2016.
 */
public class HttpHelper {
    public static void getOrderHistory(final Context context, final String TAG) {
        Utilities.sendNormalLock(true, context, TAG);
        final Token token = CRUDHelper.getToken(context);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", token.getToken());
        GsonRequest<OrderItem> mGsonRequest = new GsonRequest<OrderItem>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Order/History?PageNo="+context.getResources().getInteger(R.integer.page_no)+"&NoPerPage=" + context.getResources().getInteger(R.integer.max_no_per_page),
                OrderItem.class,
                headers,
                responseOrderListener(context, TAG),
                errorOrderListenr(context, TAG)
        ) {

        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<OrderItem> responseOrderListener(final Context context, final String TAG) {
        return new Response.Listener<OrderItem>() {
            @Override
            public void onResponse(OrderItem response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("Order", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorOrderListenr(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void getOrderHistoryDetails(final Context context, final String TAG, String id) {
        Utilities.sendNormalLock(true, context, TAG);
        final Token token = CRUDHelper.getToken(context);
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", token.getToken());
        GsonRequest<OrderItem> mGsonRequest = new GsonRequest<OrderItem>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "Order/Single?orderId=" + id,
                OrderItem.class,
                headers,
                responseOrderDetailsListener(context, TAG),
                errorOrderDetailsListenr(context, TAG)
        ) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<OrderItem> responseOrderDetailsListener(final Context context, final String TAG) {
        return new Response.Listener<OrderItem>() {
            @Override
            public void onResponse(OrderItem response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("Order", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorOrderDetailsListenr(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
