package com.applab.goodmorning.Order.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.applab.goodmorning.Order.adapter.OrderDetailsAdapter;
import com.applab.goodmorning.Order.model.Order;
import com.applab.goodmorning.Order.model.OrderItem;
import com.applab.goodmorning.Order.webapi.HttpHelper;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Welcome.model.Products;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * -- =============================================
 * -- Author     : Muhammad Izzun Mustaqim Bin Ismahdi
 * -- Create date: 9/3/2016
 * -- Description: OrderHistoryDetailsActivity .java
 * -- =============================================
 * HISTORY OF UPDATE
 * <p>
 * NO     DEVELOPER         DATETIME                      DESCRIPTION
 * *******************************************************************************
 * 1
 * 2
 */

public class OrderHistoryDetailsActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private ImageButton mImg;
    private OrderDetailsAdapter mAdapter;
    private Order mOrder;

    private TextView mTxtDate, mTxtOrderCode, mTxtStatus, mTxtSubTotal, mTxtShippingFee,
            mTxtDiscount, mTxtGST, mTxtTotal, mTxtPromotionalCode, mTxtTracking, mTxtDeliveryCourier;
    private LinearLayout mLnPromotionCode, mLvDiscount;
    private LinearLayout mLvGst;
    private String TAG = OrderHistoryDetailsActivity.class.getSimpleName();
    private View mViewOrderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_details);

        mOrder = getIntent().getParcelableExtra("OrderDetails");

        mToolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_order_history));
        mViewOrderList = findViewById(R.id.viewOrderList);
        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mImg = (ImageButton) mToolbar.findViewById(R.id.img);
        mImg.setVisibility(View.GONE);

        setData();

        mRecyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new OrderDetailsAdapter(OrderHistoryDetailsActivity.this, mOrder);
        mLayoutManager = new LinearLayoutManager(OrderHistoryDetailsActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_empty, menu);
        return true;
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    public void setData() {
        mTxtDate = (TextView) mViewOrderList.findViewById(R.id.txtDate);
        mTxtOrderCode = (TextView) mViewOrderList.findViewById(R.id.txtOrderCode);
        mTxtStatus = (TextView) mViewOrderList.findViewById(R.id.txtStatus);
        mTxtSubTotal = (TextView) findViewById(R.id.txtSubTotal);
        mTxtShippingFee = (TextView) findViewById(R.id.txtShippingFee);
        mTxtDiscount = (TextView) findViewById(R.id.txtDiscount);
        mTxtGST = (TextView) findViewById(R.id.txtGST);
        mTxtTotal = (TextView) findViewById(R.id.txtTotal);
        mTxtPromotionalCode = (TextView) findViewById(R.id.txtPromotionalCode);
        mTxtTracking = (TextView) mViewOrderList.findViewById(R.id.txtTracking);
        mTxtDeliveryCourier = (TextView) mViewOrderList.findViewById(R.id.txtDeliveryCourier);
        mLnPromotionCode = (LinearLayout) findViewById(R.id.lnPromotionalCode);
        mLvGst = (LinearLayout) findViewById(R.id.lvGst);
        mLvDiscount = (LinearLayout) findViewById(R.id.lvDiscount);

        mTxtDate.setText(Utilities.setCalendarDate(Utilities.DATE_FORMAT,
                "yyyy-MM-dd", mOrder.getCreateDate()));
        mTxtOrderCode.setText(mOrder.getOrderId());
        mTxtStatus.setText(mOrder.getStatus());
        mTxtSubTotal.setText(mOrder.getCurrency() + String.valueOf(Utilities.convertToDecimal(mOrder.getTotalPrice(), 2)));
        mTxtShippingFee.setText(mOrder.getCurrency() + String.valueOf(Utilities.convertToDecimal(mOrder.getShippingPrice(), 2)));

        mTxtDeliveryCourier.setText(mOrder.getDeliveryCourier());
        mTxtTracking.setText(mOrder.getTrackingNumber());
        mLvGst.setVisibility(View.GONE);
        /*if (mOrder.getGstPrice() == 0) {
            mLvGst.setVisibility(View.GONE);
        } else {
            mLvGst.setVisibility(View.VISIBLE);
            mTxtGST.setText(mOrder.getCurrency() + String.valueOf(mOrder.getGstPrice()));
        }*/

        mTxtTotal.setText(String.valueOf(Utilities.convertToDecimal(mOrder.getTotalPriceIncGst(), 2)));

        if (mOrder.getPromotionalCode() == null) {
            mLvDiscount.setVisibility(View.GONE);
            mTxtDiscount.setText(mOrder.getCurrency() + " 0.00");
        } else {
            if (mOrder.getPromotionalCode().length() > 0) {
                mTxtPromotionalCode.setText(" " + mOrder.getPromotionalCode());
                mTxtDiscount.setText(mOrder.getCurrency() + String.valueOf(Utilities.convertToDecimal(mOrder.getPromotionalValue(), 2)));
            } else {
                mLvDiscount.setVisibility(View.GONE);
                mTxtDiscount.setText(mOrder.getCurrency() + " 0.00");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        HttpHelper.getOrderHistoryDetails(OrderHistoryDetailsActivity.this, TAG, mOrder.getOrderId());
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getParcelableExtra("Order") != null) {
                    OrderItem orderItem = intent.getParcelableExtra("Order");
                    if (orderItem != null) {
                        if (orderItem.getItems() != null) {
                            mOrder = new ArrayList<>(orderItem.getItems()).get(0);
                            setData();
                        }
                    }
                } else if (intent.getIntExtra("isNormalLock", 2) == 1) {
                    Utilities.setNormalLock(true, OrderHistoryDetailsActivity.this, findViewById(R.id.fadeProgress));
                } else if (intent.getIntExtra("isNormalLock", 2) == 0) {
                    Utilities.setNormalLock(false, OrderHistoryDetailsActivity.this, findViewById(R.id.fadeProgress));
                }
            }
        }
    };
}
