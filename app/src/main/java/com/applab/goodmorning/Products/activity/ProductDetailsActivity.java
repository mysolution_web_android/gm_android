package com.applab.goodmorning.Products.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applab.goodmorning.Checkout.activity.CartActivity;
import com.applab.goodmorning.Checkout.webapi.HttpHelper;
import com.applab.goodmorning.Image.activity.ImageSlidingActivty;
import com.applab.goodmorning.Login.database.CRUDHelper;
import com.applab.goodmorning.Products.adapter.ProductDetailsAdapter;
import com.applab.goodmorning.Products.model.Product;
import com.applab.goodmorning.Products.model.ProductItems;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Register.model.Country;
import com.applab.goodmorning.Register.provider.CountryProvider;
import com.applab.goodmorning.Utilities.DBHelper;
import com.applab.goodmorning.Utilities.ItemClickSupport;
import com.applab.goodmorning.Utilities.Utilities;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;


import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity implements SwipyRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {

    private Toolbar mToolbar;
    private TextView mTxtToolbarTitle;
    private View mSideMenu;

    private TextView mTxtProductName;
    private TextView mTxtProductWeight;
    private TextView mTxtProductDesc;
    private LinearLayout mBtnAddToCart;
    private TextView mTxtProductPrice;

    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerview;
    private ProductDetailsAdapter mAdapter;

    private String TAG = ProductDetailsActivity.class.getSimpleName();
    private Product mProduct;
    private int mId = 0;
    private View mActionShop;

    private ImageView mImgProduct;
    private int mLoaderCountryId = 234;
    private TextView mTxtGST;
    private SwipyRefreshLayout mSwipyRefreshLayout;

    private RelativeLayout mBtnProductOverView, mBtnMainIngredients,
            mBtnRecommendedFor, mBtnBenefits, mBtnDirectionsForUse, mBtnCertificates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        mToolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(mToolbar);
        mTxtToolbarTitle = (TextView) mToolbar.findViewById(R.id.txtTitle);
        mTxtToolbarTitle.setText(getString(R.string.title_activity_products));

        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerView);
        mImgProduct = (ImageView) findViewById(R.id.imgProduct);
        mRecyclerview.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ProductDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);

        mAdapter = new ProductDetailsAdapter(ProductDetailsActivity.this, mProduct, mImgProduct);
        mRecyclerview.setAdapter(mAdapter);
        mRecyclerview.setLayoutManager(mLayoutManager);

        mSideMenu = (View) findViewById(R.id.sideMenu);
        Utilities.setSideMenuOnClickListener(this, mSideMenu);

        mToolbar.setNavigationIcon(R.mipmap.back);
        mToolbar.setNavigationOnClickListener(mToolbarOnClickListener);

        mSwipyRefreshLayout = (SwipyRefreshLayout) findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(this);

        mTxtProductName = (TextView) findViewById(R.id.txtProductName);
        mTxtProductWeight = (TextView) findViewById(R.id.txtProductWeight);
        mTxtProductDesc = (TextView) findViewById(R.id.txtDesc);
        mTxtProductPrice = (TextView) findViewById(R.id.txtProductPrice);
        mImgProduct = (ImageView) findViewById(R.id.imgProducts);
        mBtnAddToCart = (LinearLayout) findViewById(R.id.btnAddToCart);
        mTxtGST = (TextView) findViewById(R.id.txtGST);

        mBtnProductOverView = (RelativeLayout) findViewById(R.id.btnProductOverview);
        mBtnMainIngredients = (RelativeLayout) findViewById(R.id.btnMainIngredients);
        mBtnRecommendedFor = (RelativeLayout) findViewById(R.id.btnRecommendFor);
        mBtnBenefits = (RelativeLayout) findViewById(R.id.btnBenefits);
        mBtnDirectionsForUse = (RelativeLayout) findViewById(R.id.btnDirectionsForUse);
        mBtnCertificates = (RelativeLayout) findViewById(R.id.btnCertificates);

        mBtnProductOverView.setOnClickListener(mBtnOverViewOnClickListener);
        mBtnMainIngredients.setOnClickListener(mBtnMainIngredientsOnClickListener);
        mBtnRecommendedFor.setOnClickListener(mBtnRecommendedForOnClickListener);
        mBtnBenefits.setOnClickListener(mBtnOnBenefitsClickListener);
        mBtnDirectionsForUse.setOnClickListener(mBtnOnDirectionClickListener);
        mBtnCertificates.setOnClickListener(mBtnOnCertificatesClickListener);
        mBtnAddToCart.setOnClickListener(mBtnAddCartOnClickListener);

        mId = getIntent().getIntExtra("id", 0);
        ItemClickSupport.addTo(mRecyclerview).setOnItemClickListener(mItemClickListener);
    }

    private ItemClickSupport.OnItemClickListener mItemClickListener = new ItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
            mAdapter.setPosition(position);
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Utilities.checkSideMenuOnPause(this, mSideMenu);
        LocalBroadcastManager.getInstance(getBaseContext()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDelay(this);
        getSupportLoaderManager().initLoader(mLoaderCountryId, null, this);
        IntentFilter iff = new IntentFilter(TAG);
        LocalBroadcastManager.getInstance(getBaseContext()).registerReceiver(broadcastReceiver, iff);
        com.applab.goodmorning.Register.webapi.HttpHelper.getCountry(ProductDetailsActivity.this, TAG);
        com.applab.goodmorning.Products.webapi.HttpHelper.getProductDetails(mId, ProductDetailsActivity.this, TAG);
    }

    private void setDelay(final Context context) {
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                Utilities.refreshActionShop(context, mActionShop);
            }
        };
        handler.postDelayed(r, 1000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_menu) {
            Utilities.setSideMenuVisible(mSideMenu);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        mActionShop = (View) menu.findItem(R.id.action_shop).getActionView();
        Utilities.setAddCartOnClickListener(ProductDetailsActivity.this, mActionShop);
        return true;
    }

    private View.OnClickListener mToolbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(TAG)) {
                if (intent.getParcelableExtra("Product") != null) {
                    setData(intent);
                } else if (intent.getIntExtra("isLock", 2) == 1) {
                    Utilities.setFadeProgressBarVisibility(true, findViewById(R.id.fadeProgress));
                } else if (intent.getIntExtra("isLock", 2) == 0) {
                    Utilities.refreshActionShop(ProductDetailsActivity.this, mActionShop);
                    Utilities.setFadeProgressBarVisibility(false, findViewById(R.id.fadeProgress));
                } else if (intent.getIntExtra("isNormalLock", 2) == 1) {
                    Utilities.setNormalLock(true, ProductDetailsActivity.this, findViewById(R.id.fadeProgress));
                } else if (intent.getIntExtra("isNormalLock", 2) == 0) {
                    Utilities.setNormalLock(false, ProductDetailsActivity.this, findViewById(R.id.fadeProgress));
                }
            }
        }
    };

    public void setData(Intent intent) {
        mProduct = intent.getParcelableExtra("Product");
        if (mProduct != null) {
            mTxtProductName.setText(mProduct.getProductTitle());
            mTxtProductWeight.setText(mProduct.getProductSubTitle());
            mTxtProductPrice.setText(mProduct.getPrice());
            mTxtProductDesc.setText(Html.fromHtml(mProduct.getProductDescription()));
            mAdapter.swapProduct(mProduct);
        }
    }

    private View.OnClickListener mBtnAddCartOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (CRUDHelper.getToken(ProductDetailsActivity.this) == null) {
                Utilities.showError(ProductDetailsActivity.this, "", getString(R.string.need_login));
            } else {
                HttpHelper.postCart(ProductDetailsActivity.this, TAG, mId);
            }
        }
    };

    private View.OnClickListener mBtnOverViewOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getProductOverview() != null) {
                        arr.add(mProduct.getProductExtras().getProductOverview());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }

        }
    };

    private View.OnClickListener mBtnMainIngredientsOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getMainIngredients() != null) {
                        arr.add(mProduct.getProductExtras().getMainIngredients());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }

        }
    };

    private View.OnClickListener mBtnRecommendedForOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getRecommendedFor() != null) {
                        arr.add(mProduct.getProductExtras().getRecommendedFor());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }

        }
    };

    private View.OnClickListener mBtnOnBenefitsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getBenefits() != null) {
                        arr.add(mProduct.getProductExtras().getBenefits());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }
        }
    };

    private View.OnClickListener mBtnOnDirectionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getDirectionsForUse() != null) {
                        arr.add(mProduct.getProductExtras().getDirectionsForUse());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }

        }
    };

    private View.OnClickListener mBtnOnCertificatesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(ProductDetailsActivity.this, ImageSlidingActivty.class);
            ArrayList<String> arr = new ArrayList<>();
            if (mProduct != null)
                if (mProduct.getProductExtras() != null)
                    if (mProduct.getProductExtras().getCertificates() != null) {
                        arr.add(mProduct.getProductExtras().getCertificates());
                        intent.putExtra("url", arr);
                        intent.putExtra("position", 0);
                        startActivity(intent);
                    }
        }
    };

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (mLoaderCountryId == id) {
            return new CursorLoader(ProductDetailsActivity.this, CountryProvider.CONTENT_URI, null, DBHelper.COUNTRY_COLUMN_COUNTRY_ID + "=?", new String[]{String.valueOf(Utilities.getCountryID(ProductDetailsActivity.this))}, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == mLoaderCountryId) {
            if (data != null) {
                if (data.getCount() > 0) {
                    Country country = Country.getCountry(data, 0);
                    if (country.getGst() > 0) {
                        mTxtGST.setVisibility(View.VISIBLE);
                        mTxtGST.setText(getString(R.string.incl_gst) + " " + country.getGst() + "%" + " " + getString(R.string.gst1));
                    } else {
                        mTxtGST.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        Log.d(TAG, "Refresh triggered at "
                + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));
        if (direction == SwipyRefreshLayoutDirection.TOP) {
            com.applab.goodmorning.Products.webapi.HttpHelper.getProductDetails(mId, ProductDetailsActivity.this, TAG);
        }
        Utilities.disableSwipyRefresh(mSwipyRefreshLayout);
    }
}
