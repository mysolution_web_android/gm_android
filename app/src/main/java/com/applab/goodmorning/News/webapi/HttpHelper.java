package com.applab.goodmorning.News.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.goodmorning.Account.provider.AccountProvider;
import com.applab.goodmorning.Login.database.CRUDHelper;
import com.applab.goodmorning.Login.provider.TokenProvider;
import com.applab.goodmorning.News.model.News;
import com.applab.goodmorning.News.model.NewsItem;
import com.applab.goodmorning.R;

import com.applab.goodmorning.Utilities.AppController;
import com.applab.goodmorning.Utilities.GsonRequest;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Utilities.dialog.GeneralDialogFragment;
import com.applab.goodmorning.Welcome.activity.WelcomeActivity;

import java.util.ArrayList;

import eu.janmuller.android.simplecropimage.Util;

/**
 * Created by User on 4/4/2016.
 */
public class HttpHelper {
    public static void getNews(final Context context, final String TAG) {
        Utilities.sendNormalLock(true, context, TAG);
        GsonRequest<NewsItem> mGsonRequest = new GsonRequest<NewsItem>(
                Request.Method.GET,
                context.getString(R.string.base_url) + "News/List?countryId=" + Utilities.getCountryID(context) + "&newsType=" + TAG + "&PageNo=" + context.getResources().getInteger(R.integer.page_no) + "&NoPerPage=" + context.getResources().getInteger(R.integer.max_no_per_page),
                NewsItem.class,
                null,
                responseNewsItemListener(context, TAG),
                errorNewsItemListenr(context, TAG)
        ) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<NewsItem> responseNewsItemListener(final Context context, final String TAG) {
        return new Response.Listener<NewsItem>() {
            @Override
            public void onResponse(NewsItem response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("News", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorNewsItemListenr(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }

    public static void getNewsDetails(final Context context, final String TAG, final int newsId) {
        Utilities.sendNormalLock(true, context, TAG);
        String url = context.getString(R.string.base_url) + "News/Single?newsId=" + newsId;
        GsonRequest<NewsItem> mGsonRequest = new GsonRequest<NewsItem>(
                Request.Method.GET,
                url,
                NewsItem.class,
                null,
                responseNewsDetailsListener(context, TAG),
                errorNewsDetailsListenr(context, TAG)
        ) {
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<NewsItem> responseNewsDetailsListener(final Context context, final String TAG) {
        return new Response.Listener<NewsItem>() {
            @Override
            public void onResponse(NewsItem response) {
                Utilities.sendNormalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Intent intent = new Intent(TAG);
                    intent.putExtra("NewsDetailsBroadcast", response);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorNewsDetailsListenr(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendNormalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
