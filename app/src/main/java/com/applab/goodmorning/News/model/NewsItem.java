package com.applab.goodmorning.News.model;

/**
 * Created by User on 4/4/2016.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsItem implements Parcelable {
    @SerializedName("SystemCode")
    @Expose
    private Integer SystemCode;
    @SerializedName("PageNo")
    @Expose
    private Integer PageNo;
    @SerializedName("NoPerPage")
    @Expose
    private Integer NoPerPage;
    @SerializedName("SystemMessage")
    @Expose
    private String SystemMessage;
    @SerializedName("SystemDebugMessage")
    @Expose
    private String SystemDebugMessage;
    @SerializedName("items")
    @Expose
    private List<News> News;

    /**
     *
     * @return
     * The SystemCode
     */
    public Integer getSystemCode() {
        return SystemCode;
    }

    /**
     *
     * @param SystemCode
     * The SystemCode
     */
    public void setSystemCode(Integer SystemCode) {
        this.SystemCode = SystemCode;
    }

    /**
     *
     * @return
     * The PageNo
     */
    public Integer getPageNo() {
        return PageNo;
    }

    /**
     *
     * @param PageNo
     * The PageNo
     */
    public void setPageNo(Integer PageNo) {
        this.PageNo = PageNo;
    }

    /**
     *
     * @return
     * The NoPerPage
     */
    public Integer getNoPerPage() {
        return NoPerPage;
    }

    /**
     *
     * @param NoPerPage
     * The NoPerPage
     */
    public void setNoPerPage(Integer NoPerPage) {
        this.NoPerPage = NoPerPage;
    }

    /**
     *
     * @return
     * The SystemMessage
     */
    public String getSystemMessage() {
        return SystemMessage;
    }

    /**
     *
     * @param SystemMessage
     * The SystemMessage
     */
    public void setSystemMessage(String SystemMessage) {
        this.SystemMessage = SystemMessage;
    }

    /**
     *
     * @return
     * The SystemDebugMessage
     */
    public String getSystemDebugMessage() {
        return SystemDebugMessage;
    }

    /**
     *
     * @param SystemDebugMessage
     * The SystemDebugMessage
     */
    public void setSystemDebugMessage(String SystemDebugMessage) {
        this.SystemDebugMessage = SystemDebugMessage;
    }

    /**
     * @return The News
     */
    public List<News> getNews() {
        return News;
    }

    /**
     * @param News The News
     */
    public void setNews(List<News> News) {
        this.News = News;
    }

    public NewsItem() {

    }

    public void readFromParcel(Parcel in) {
        this.News = in.readArrayList(News.getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.SystemCode);
        dest.writeValue(this.PageNo);
        dest.writeValue(this.NoPerPage);
        dest.writeString(this.SystemMessage);
        dest.writeString(this.SystemDebugMessage);
        dest.writeTypedList(News);
    }

    protected NewsItem(Parcel in) {
        this.SystemCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.PageNo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.NoPerPage = (Integer) in.readValue(Integer.class.getClassLoader());
        this.SystemMessage = in.readString();
        this.SystemDebugMessage = in.readString();
        this.News = in.createTypedArrayList(com.applab.goodmorning.News.model.News.CREATOR);
    }

    public static final Creator<NewsItem> CREATOR = new Creator<NewsItem>() {
        @Override
        public NewsItem createFromParcel(Parcel source) {
            return new NewsItem(source);
        }

        @Override
        public NewsItem[] newArray(int size) {
            return new NewsItem[size];
        }
    };
}
