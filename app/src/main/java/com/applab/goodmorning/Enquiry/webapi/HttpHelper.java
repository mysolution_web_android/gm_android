package com.applab.goodmorning.Enquiry.webapi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.applab.goodmorning.ContactUs.model.ContactUsItem;
import com.applab.goodmorning.Enquiry.activity.EnquiryActivity;
import com.applab.goodmorning.Enquiry.model.Enquiry;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.AppController;
import com.applab.goodmorning.Utilities.GsonRequest;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Utilities.dialog.GeneralDialogFragment;
import com.google.gson.Gson;

import org.apache.http.entity.ContentType;

/**
 * Created by user on 12/4/2016.
 */
public class HttpHelper {
    public static void postEnquiry(Context context, String TAG, final Enquiry enquiry, String baseUrl) {
        Utilities.sendLocalLock(true, context, TAG);
        GsonRequest<ContactUsItem> mGsonRequest = new GsonRequest<ContactUsItem>(
                Request.Method.POST,
                context.getString(R.string.base_url) + baseUrl,
                ContactUsItem.class,
                null,
                responseEventListListener(context, TAG),
                errorEventListListener(context, TAG)) {

            @Override
            public String getBodyContentType() {
                return ContentType.APPLICATION_JSON.toString();
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return new Gson().toJson(enquiry).getBytes();
            }
        };
        mGsonRequest.setRetryPolicy(new DefaultRetryPolicy(Utilities.TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mGsonRequest, TAG);
    }

    public static Response.Listener<ContactUsItem> responseEventListListener(final Context context, final String TAG) {
        return new Response.Listener<ContactUsItem>() {
            @Override
            public void onResponse(ContactUsItem response) {
                Utilities.sendLocalLock(false, context, TAG);
                if (response.getSystemCode() == 200) {
                    Utilities.showError(context, "", context.getString(R.string.success_send));
                } else {
                    GeneralDialogFragment generalDialogFragment = GeneralDialogFragment.newInstance(context,
                            response.getSystemMessage(), context.getString(R.string.warning));
                    generalDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                }
            }
        };
    }

    public static Response.ErrorListener errorEventListListener(final Context context, final String TAG) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utilities.sendLocalLock(false, context, TAG);
                Utilities.serverHandlingError(context, error);
            }
        };
    }
}
