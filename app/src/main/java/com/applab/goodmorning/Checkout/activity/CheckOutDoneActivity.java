package com.applab.goodmorning.Checkout.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.applab.goodmorning.Order.activity.OrderHistoryActivity;
import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Welcome.activity.WelcomeActivity;

public class CheckOutDoneActivity extends AppCompatActivity {
    private Button mBtnHome;
    private Button mBtnOrderHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out_done);
        String url = getIntent().getStringExtra("url");
        Utilities.openWeb(this, url);
        mBtnHome = (Button) findViewById(R.id.btnHome);
        mBtnOrderHistory = (Button) findViewById(R.id.btnOrderHistory);
        mBtnHome.setOnClickListener(mBtnHomeOnClickListener);
        mBtnOrderHistory.setOnClickListener(mBtnOrderHistoryOnClickListener);
    }

    private View.OnClickListener mBtnHomeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CheckOutDoneActivity.this, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    };

    private View.OnClickListener mBtnOrderHistoryOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(CheckOutDoneActivity.this, OrderHistoryActivity.class);
            startActivity(intent);
            finish();
        }
    };

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }
}
