package com.applab.goodmorning.HomeScreen.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.applab.goodmorning.R;
import com.applab.goodmorning.Utilities.Utilities;
import com.applab.goodmorning.Welcome.activity.WelcomeActivity;
import com.vistrav.ask.Ask;
import com.vistrav.ask.annotations.AskDenied;
import com.vistrav.ask.annotations.AskGranted;

public class SplashScreenActivity extends AppCompatActivity {
    Handler timeHandler = new Handler();
    private String languageToLoad = "en_US";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utilities.isNotPermitted(this)) {
            setUpHomeScreen();
        }
    }

    @AskGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void fileAccessGranted() {
        setUpHomeScreen();
    }

    @AskDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void fileAccessDenied() {
        Utilities.showError(SplashScreenActivity.this, "", getString(R.string.you_dont_have));
        finish();
    }

    private void setUpHomeScreen() {
        timeHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utilities.getLanguage(SplashScreenActivity.this).equals("")) {
                    Utilities.saveLanguage(languageToLoad, SplashScreenActivity.this);
                }
                Utilities.loadLanguage(SplashScreenActivity.this);
                Intent intent = new Intent(SplashScreenActivity.this, WelcomeActivity.class);
                startActivity(intent);
            }
        }, 1000);
    }
}
